#include "imgui_sfml_impl.hpp"
#include "imgui/imgui.h"
#include <SFML/Window.hpp>
#include <string>

namespace
{
	std::string clipboardData;
	sf::Cursor cursors[ImGuiMouseCursor_COUNT];
	bool mouseButtonsPressed[sf::Mouse::Button::ButtonCount]{};
	bool mouseButtonsReleased[sf::Mouse::Button::ButtonCount]{};

	void setClipboardText(void *, const char *data)
	{
		sf::Clipboard::setString(data);
	}

	const char *getClipboardText(void *)
	{
		clipboardData = sf::Clipboard::getString().toAnsiString();
		return clipboardData.c_str();
	}
} // namespace

void ImGui_ImplSFML_init()
{
	// Setup Platform/Renderer bindings
	ImGuiIO &io = ImGui::GetIO();

	io.BackendFlags |= ImGuiBackendFlags_HasMouseCursors;
	cursors[ImGuiMouseCursor_Arrow].loadFromSystem(sf::Cursor::Arrow);
	cursors[ImGuiMouseCursor_TextInput].loadFromSystem(sf::Cursor::Text);
	cursors[ImGuiMouseCursor_ResizeAll].loadFromSystem(sf::Cursor::SizeAll);
	cursors[ImGuiMouseCursor_ResizeNS].loadFromSystem(sf::Cursor::SizeVertical);
	cursors[ImGuiMouseCursor_ResizeEW].loadFromSystem(sf::Cursor::SizeHorizontal);
	cursors[ImGuiMouseCursor_ResizeNESW].loadFromSystem(sf::Cursor::SizeBottomLeftTopRight);
	cursors[ImGuiMouseCursor_ResizeNWSE].loadFromSystem(sf::Cursor::SizeTopLeftBottomRight);
	cursors[ImGuiMouseCursor_Hand].loadFromSystem(sf::Cursor::Hand);
	cursors[ImGuiMouseCursor_NotAllowed].loadFromSystem(sf::Cursor::NotAllowed);

	io.BackendFlags |= ImGuiBackendFlags_HasSetMousePos;
	io.BackendPlatformName = "imgui_sfml_impl";
	io.KeyMap[ImGuiKey_Tab] = sf::Keyboard::Tab;
	io.KeyMap[ImGuiKey_LeftArrow] = sf::Keyboard::Left;
	io.KeyMap[ImGuiKey_RightArrow] = sf::Keyboard::Right;
	io.KeyMap[ImGuiKey_UpArrow] = sf::Keyboard::Up;
	io.KeyMap[ImGuiKey_DownArrow] = sf::Keyboard::Down;
	io.KeyMap[ImGuiKey_PageUp] = sf::Keyboard::PageUp;
	io.KeyMap[ImGuiKey_PageDown] = sf::Keyboard::PageDown;
	io.KeyMap[ImGuiKey_Home] = sf::Keyboard::Home;
	io.KeyMap[ImGuiKey_End] = sf::Keyboard::End;
	io.KeyMap[ImGuiKey_Insert] = sf::Keyboard::Insert;
	io.KeyMap[ImGuiKey_Delete] = sf::Keyboard::Delete;
	io.KeyMap[ImGuiKey_Backspace] = sf::Keyboard::Backspace;
	io.KeyMap[ImGuiKey_Space] = sf::Keyboard::Space;
	io.KeyMap[ImGuiKey_Enter] = sf::Keyboard::Enter;
	io.KeyMap[ImGuiKey_Escape] = sf::Keyboard::Escape;
	io.KeyMap[ImGuiKey_A] = sf::Keyboard::A;
	io.KeyMap[ImGuiKey_C] = sf::Keyboard::C;
	io.KeyMap[ImGuiKey_V] = sf::Keyboard::V;
	io.KeyMap[ImGuiKey_X] = sf::Keyboard::X;
	io.KeyMap[ImGuiKey_Y] = sf::Keyboard::Y;
	io.KeyMap[ImGuiKey_Z] = sf::Keyboard::Z;
	io.SetClipboardTextFn = &setClipboardText;
	io.GetClipboardTextFn = &getClipboardText;
}

void ImGui_ImplSFML_ProcessEvent(const sf::Event &event)
{
	ImGuiIO &io = ImGui::GetIO();
	switch(event.type)
	{
		case sf::Event::MouseWheelScrolled:
			switch(event.mouseWheelScroll.wheel)
			{
				case sf::Mouse::Wheel::HorizontalWheel:
					io.MouseWheelH += event.mouseWheelScroll.delta;
					break;
				case sf::Mouse::Wheel::VerticalWheel:
					io.MouseWheel += event.mouseWheelScroll.delta;
					break;
			}
			break;
		case sf::Event::MouseButtonPressed:
			mouseButtonsPressed[event.mouseButton.button] = true;
			break;
		case sf::Event::MouseButtonReleased:
			mouseButtonsReleased[event.mouseButton.button] = true;
			break;
		case sf::Event::TextEntered:
			io.AddInputCharacter(event.text.unicode);
			break;
		case sf::Event::KeyPressed:
			io.KeysDown[event.key.code] = true;
			switch(event.key.code)
			{
				case sf::Keyboard::RShift:
				case sf::Keyboard::LShift:
					io.KeyShift = true;
					break;
				case sf::Keyboard::RControl:
				case sf::Keyboard::LControl:
					io.KeyCtrl = true;
					break;
				case sf::Keyboard::RAlt:
				case sf::Keyboard::LAlt:
					io.KeyAlt = true;
					break;
				case sf::Keyboard::LSystem:
				case sf::Keyboard::RSystem:
					io.KeySuper = true;
					break;
				default:
					break;
			}
			break;
		case sf::Event::KeyReleased:
			io.KeysDown[event.key.code] = false;
			switch(event.key.code)
			{
				case sf::Keyboard::RShift:
				case sf::Keyboard::LShift:
					io.KeyShift = false;
					break;
				case sf::Keyboard::RControl:
				case sf::Keyboard::LControl:
					io.KeyCtrl = false;
					break;
				case sf::Keyboard::RAlt:
				case sf::Keyboard::LAlt:
					io.KeyAlt = false;
					break;
				case sf::Keyboard::LSystem:
				case sf::Keyboard::RSystem:
					io.KeySuper = false;
					break;
				default:
					break;
			}
			break;
		case sf::Event::Resized:
			io.DisplaySize = ImVec2(static_cast<float>(event.size.width), static_cast<float>(event.size.height));
			break;
		default:
			break;
	}
}

void ImGui_ImplSFML_NewFrame(sf::Window &window)
{
	ImGuiIO &io = ImGui::GetIO();
	for(unsigned int i = 0; i < sf::Mouse::ButtonCount; ++i)
	{
		io.MouseDown[i] = mouseButtonsPressed[i];
		if(mouseButtonsReleased[i])
		{
			mouseButtonsPressed[i] = false;
		}
		mouseButtonsReleased[i] = false;
	}
	window.setMouseCursor(cursors[ImGui::GetMouseCursor()]);
	
	if(io.WantSetMousePos)
	{
		sf::Mouse::setPosition({static_cast<int>(io.MousePos.x), static_cast<int>(io.MousePos.y)}, window);
	}
	const auto mousepos = sf::Mouse::getPosition(window);
	io.MousePos = ImVec2(static_cast<float>(mousepos.x), static_cast<float>(mousepos.y));
	if(io.MouseDrawCursor || ImGui::GetMouseCursor() == ImGuiMouseCursor_None)
	{
		window.setMouseCursorVisible(false);
	}
	else
	{
		window.setMouseCursorVisible(true);
	}
}

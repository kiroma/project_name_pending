#include <SFML/Window/Event.hpp>

void ImGui_ImplSFML_init();
void ImGui_ImplSFML_ProcessEvent(const sf::Event &event);
void ImGui_ImplSFML_NewFrame(sf::Window &window);

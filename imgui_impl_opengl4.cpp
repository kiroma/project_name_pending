#include "imgui_impl_opengl4.h"
#include <cstdio>

// OpenGL Data
namespace
{
	GLuint g_ShaderHandle = 0;
	GLuint g_vertex_array_object = 0;
	// Attribute and uniform locations are explicit. Hardcode them in.
	constexpr GLint g_AttribLocationTex = 1, g_AttribLocationProjMtx = 0;
	constexpr GLint g_AttribLocationVtxPos = 0, g_AttribLocationVtxUV = 1, g_AttribLocationVtxColor = 2;
	unsigned int g_VboHandle = 0, g_ElementsHandle = 0;
	unsigned long g_ebosize = 0, g_vbosize = 0;
} // namespace

// Functions
bool ImGui_ImplOpenGL3_Init()
{
	// Setup back-end capabilities flags
	ImGuiIO &io = ImGui::GetIO();
	io.BackendRendererName = "imgui_impl_opengl4";
	io.BackendFlags |= ImGuiBackendFlags_RendererHasVtxOffset; // We can honor the ImDrawCmd::VtxOffset field, allowing for large meshes.
	return true;
}

void ImGui_ImplOpenGL3_Shutdown()
{
	ImGui_ImplOpenGL3_DestroyDeviceObjects();
}

void ImGui_ImplOpenGL3_NewFrame()
{
	if(!g_ShaderHandle)
		ImGui_ImplOpenGL3_CreateDeviceObjects();
}

static void ImGui_ImplOpenGL3_SetupRenderState(ImDrawData *draw_data, int fb_width, int fb_height, GLuint &vertex_array_object)
{
	// Setup render state: alpha-blending enabled, no face culling, no depth testing, scissor enabled, polygon fill
	glEnable(GL_BLEND);
	glBlendEquation(GL_FUNC_ADD);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_SCISSOR_TEST);
#ifdef GL_POLYGON_MODE
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
#endif

	// Setup viewport, orthographic projection matrix
	// Our visible imgui space lies from draw_data->DisplayPos (top left) to draw_data->DisplayPos+data_data->DisplaySize (bottom right). DisplayPos is (0,0) for single viewport apps.
	glViewport(0, 0, fb_width, fb_height);
	float L = draw_data->DisplayPos.x;
	float R = draw_data->DisplayPos.x + draw_data->DisplaySize.x;
	float T = draw_data->DisplayPos.y;
	float B = draw_data->DisplayPos.y + draw_data->DisplaySize.y;
	const float ortho_projection[4][4] =
	{
	{2.0f / (R - L), 0.0f, 0.0f, 0.0f},
	{0.0f, 2.0f / (T - B), 0.0f, 0.0f},
	{0.0f, 0.0f, -1.0f, 0.0f},
	{(R + L) / (L - R), (T + B) / (B - T), 0.0f, 1.0f},
	};
	glUseProgram(g_ShaderHandle);
	glUniform1i(g_AttribLocationTex, 0);
	glUniformMatrix4fv(g_AttribLocationProjMtx, 1, GL_FALSE, &ortho_projection[0][0]);
#ifdef GL_SAMPLER_BINDING
	glBindSampler(0, 0); // We use combined texture/sampler state. Applications using GL 3.3 may set that otherwise.
#endif

	if(!vertex_array_object)
	{
		glGenVertexArrays(1, &vertex_array_object);
		glBindVertexArray(vertex_array_object);
		// Bind vertex/index buffers and setup attributes for ImDrawVert
		glBindBuffer(GL_ARRAY_BUFFER, g_VboHandle);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, g_ElementsHandle);
		glEnableVertexAttribArray(g_AttribLocationVtxPos);
		glEnableVertexAttribArray(g_AttribLocationVtxUV);
		glEnableVertexAttribArray(g_AttribLocationVtxColor);
		glVertexAttribPointer(g_AttribLocationVtxPos, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert), reinterpret_cast<void *>(offsetof(ImDrawVert, pos)));
		glVertexAttribPointer(g_AttribLocationVtxUV, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert), reinterpret_cast<void *>(offsetof(ImDrawVert, uv)));
		glVertexAttribPointer(g_AttribLocationVtxColor, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(ImDrawVert), reinterpret_cast<void *>(offsetof(ImDrawVert, col)));
	}
	else
	{
		glBindVertexArray(vertex_array_object);
		glBindBuffer(GL_ARRAY_BUFFER, g_VboHandle);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, g_ElementsHandle);
	}
}

// OpenGL3 Render function.
// (this used to be set in io.RenderDrawListsFn and called by ImGui::Render(), but you can now call this directly from your main loop)
// Note that this implementation is little overcomplicated because we are saving/setting up/restoring every OpenGL state explicitly, in order to be able to run within any OpenGL engine that doesn't do so.

void ImGui_ImplOpenGL3_RenderDrawData(ImDrawData *draw_data)
{
	// Avoid rendering when minimized, scale coordinates for retina displays (screen coordinates != framebuffer coordinates)
	int fb_width = static_cast<int>(draw_data->DisplaySize.x * draw_data->FramebufferScale.x);
	int fb_height = static_cast<int>(draw_data->DisplaySize.y * draw_data->FramebufferScale.y);
	if(fb_width <= 0 || fb_height <= 0)
		return;

	// Backup GL state
	glActiveTexture(GL_TEXTURE0);
	GLboolean last_enable_cull_face = glIsEnabled(GL_CULL_FACE);
	GLboolean last_enable_depth_test = glIsEnabled(GL_DEPTH_TEST);
	GLboolean last_enable_scissor_test = glIsEnabled(GL_SCISSOR_TEST);
	bool clip_origin_lower_left = true;
#if defined(GL_CLIP_ORIGIN) && !defined(__APPLE__)
	GLint last_clip_origin = 0;
	glGetIntegerv(GL_CLIP_ORIGIN, &last_clip_origin); // Support for GL 4.5's glClipControl(GL_UPPER_LEFT)
	if(last_clip_origin == GL_UPPER_LEFT)
		clip_origin_lower_left = false;
#endif

	ImGui_ImplOpenGL3_SetupRenderState(draw_data, fb_width, fb_height, g_vertex_array_object);

	// Will project scissor/clipping rectangles into framebuffer space
	ImVec2 clip_off = draw_data->DisplayPos; // (0,0) unless using multi-viewports
	ImVec2 clip_scale = draw_data->FramebufferScale; // (1,1) unless using retina display which are often (2,2)

	// Render command lists
	for(int n = 0; n < draw_data->CmdListsCount; n++)
	{
		const ImDrawList *cmd_list = draw_data->CmdLists[n];

		// Upload vertex/index buffers
		const unsigned long vbufsize = static_cast<unsigned long>(cmd_list->VtxBuffer.Size);
		if(vbufsize > g_vbosize)
		{
			glBufferData(GL_ARRAY_BUFFER, static_cast<GLsizeiptr>(static_cast<unsigned int>(cmd_list->VtxBuffer.Size) * sizeof(ImDrawVert)), cmd_list->VtxBuffer.Data, GL_DYNAMIC_DRAW);
			g_vbosize = vbufsize;
		}
		else
		{
			glBufferSubData(GL_ARRAY_BUFFER, 0, static_cast<GLsizeiptr>(vbufsize * sizeof(ImDrawVert)), cmd_list->VtxBuffer.Data);
		}
		const unsigned long ebufsize = static_cast<unsigned long>(cmd_list->IdxBuffer.Size);
		if(ebufsize > g_ebosize)
		{
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, static_cast<GLsizeiptr>(static_cast<unsigned int>(cmd_list->IdxBuffer.Size) * sizeof(ImDrawIdx)), cmd_list->IdxBuffer.Data, GL_DYNAMIC_DRAW);
			g_ebosize = ebufsize;
		}
		else
		{
			glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, static_cast<GLsizeiptr>(static_cast<unsigned long>(ebufsize) * sizeof(ImDrawIdx)), cmd_list->IdxBuffer.Data);
		}

		for(int cmd_i = 0; cmd_i < cmd_list->CmdBuffer.Size; cmd_i++)
		{
			const ImDrawCmd &pcmd = cmd_list->CmdBuffer[cmd_i];
			if(pcmd.UserCallback != NULL)
			{
				// User callback, registered via ImDrawList::AddCallback()
				// (ImDrawCallback_ResetRenderState is a special callback value used by the user to request the renderer to reset render state.)
				if(pcmd.UserCallback == ImDrawCallback_ResetRenderState)
					ImGui_ImplOpenGL3_SetupRenderState(draw_data, fb_width, fb_height, g_vertex_array_object);
				else
					pcmd.UserCallback(cmd_list, &pcmd);
			}
			else
			{
				// Project scissor/clipping rectangles into framebuffer space
				int clipx, clipy, clipw, cliph;
				clipx = static_cast<int>((pcmd.ClipRect.x - clip_off.x) * clip_scale.x);
				clipy = static_cast<int>((pcmd.ClipRect.y - clip_off.y) * clip_scale.y);
				clipw = static_cast<int>((pcmd.ClipRect.z - clip_off.x) * clip_scale.x);
				cliph = static_cast<int>((pcmd.ClipRect.w - clip_off.y) * clip_scale.y);

				if(clipx < fb_width && clipy < fb_height && clipw >= 0 && cliph >= 0)
				{
					// Apply scissor/clipping rectangle
					if(clip_origin_lower_left)
						glScissor(clipx, fb_height - cliph, clipw - clipx, cliph - clipy);
					else
						glScissor(clipx, clipy, clipw, cliph); // Support for GL 4.5 rarely used glClipControl(GL_UPPER_LEFT)

					// Bind texture, Draw
					glBindTexture(GL_TEXTURE_2D, pcmd.TextureId);
					glDrawElementsBaseVertex(GL_TRIANGLES, static_cast<GLsizei>(pcmd.ElemCount), sizeof(ImDrawIdx) == 2 ? GL_UNSIGNED_SHORT : GL_UNSIGNED_INT, reinterpret_cast<void *>(pcmd.IdxOffset * sizeof(ImDrawIdx)), static_cast<GLint>(pcmd.VtxOffset));
				}
			}
		}
	}

	// Restore modified GL state
	if(last_enable_cull_face)
		glEnable(GL_CULL_FACE);
	else
		glDisable(GL_CULL_FACE);
	if(last_enable_depth_test)
		glEnable(GL_DEPTH_TEST);
	else
		glDisable(GL_DEPTH_TEST);
	if(last_enable_scissor_test)
		glEnable(GL_SCISSOR_TEST);
	else
		glDisable(GL_SCISSOR_TEST);
}

bool ImGui_ImplOpenGL3_CreateFontsTexture()
{
	// Build texture atlas
	ImGuiIO &io = ImGui::GetIO();
	unsigned char *pixels;
	int width, height;
	io.Fonts->GetTexDataAsRGBA32(&pixels, &width, &height); // Load as RGBA 32-bit (75% of the memory is wasted, but default font is so small) because it is more likely to be compatible with user's existing shaders. If your ImTextureId represent a higher-level concept than just a GL texture id, consider calling GetTexDataAsAlpha8() instead to save on GPU memory.

	// Upload texture to graphics system
	GLuint &FontTexture = io.Fonts->TexID;
	glGenTextures(1, &FontTexture);
	glBindTexture(GL_TEXTURE_2D, FontTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
#ifdef GL_UNPACK_ROW_LENGTH
	glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
#endif
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA2, width, height);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	io.Fonts->ClearTexData();

	// Store our identifier

	return true;
}

void ImGui_ImplOpenGL3_DestroyFontsTexture()
{
	GLuint &FontTexture = ImGui::GetIO().Fonts->TexID;
	if(FontTexture)
	{
		glDeleteTextures(1, &FontTexture);
		FontTexture = 0;
	}
}

// If you get an error please report on github. You may try different GL context version or GLSL version. See GL<>GLSL version table at the top of this file.
static bool CheckShader(GLuint handle, const char *desc)
{
	GLint status = 0, log_length = 0;
	glGetShaderiv(handle, GL_COMPILE_STATUS, &status);
	glGetShaderiv(handle, GL_INFO_LOG_LENGTH, &log_length);
	if(status == GL_FALSE)
		fprintf(stderr, "ERROR: ImGui_ImplOpenGL3_CreateDeviceObjects: failed to compile %s!\n", desc);
	if(log_length > 1)
	{
		ImVector<char> buf;
		buf.resize(log_length + 1);
		glGetShaderInfoLog(handle, log_length, NULL, buf.begin());
		fprintf(stderr, "%s\n", buf.begin());
	}
	return status == GL_TRUE;
}

// If you get an error please report on GitHub. You may try different GL context version or GLSL version.
static bool CheckProgram(GLuint handle, const char *desc)
{
	GLint status = 0, log_length = 0;
	glGetProgramiv(handle, GL_LINK_STATUS, &status);
	glGetProgramiv(handle, GL_INFO_LOG_LENGTH, &log_length);
	if(status == GL_FALSE)
		fprintf(stderr, "ERROR: ImGui_ImplOpenGL3_CreateDeviceObjects: failed to link!\n%s\n", desc);
	if(log_length > 1)
	{
		ImVector<char> buf;
		buf.resize(log_length + 1);
		glGetProgramInfoLog(handle, log_length, NULL, buf.begin());
		fprintf(stderr, "%s\n", buf.begin());
	}
	return status == GL_TRUE;
}

bool ImGui_ImplOpenGL3_CreateDeviceObjects()
{
	static constexpr const GLchar *vertex_shader = R"glsl(
#version 430 core
layout (location = 0) in vec2 Position;
layout (location = 1) in vec2 UV;
layout (location = 2) in vec4 Color;
layout (location = 0) uniform mat4 ProjMtx;
layout (location = 0) out vec2 Frag_UV;
layout (location = 1) out vec4 Frag_Color;
void main()
{
    Frag_UV = UV;
    Frag_Color = Color;
    gl_Position = ProjMtx * vec4(Position.xy,0,1);
}
)glsl";

	static constexpr const GLchar *fragment_shader = R"glsl(
#version 430 core
layout (location = 0) in vec2 Frag_UV;
layout (location = 1) in vec4 Frag_Color;
layout (location = 1) uniform sampler2D Texture;
layout (location = 0) out vec4 Out_Color;
void main()
{
    Out_Color = Frag_Color * texture(Texture, Frag_UV.st);
}
)glsl";

	GLuint VertHandle = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(VertHandle, 1, &vertex_shader, NULL);
	glCompileShader(VertHandle);
	CheckShader(VertHandle, "vertex shader");

	GLuint FragHandle = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(FragHandle, 1, &fragment_shader, NULL);
	glCompileShader(FragHandle);
	CheckShader(FragHandle, "fragment shader");

	g_ShaderHandle = glCreateProgram();
	glAttachShader(g_ShaderHandle, VertHandle);
	glAttachShader(g_ShaderHandle, FragHandle);
	glLinkProgram(g_ShaderHandle);
	CheckProgram(g_ShaderHandle, "shader program");

	glDetachShader(g_ShaderHandle, VertHandle);
	glDetachShader(g_ShaderHandle, FragHandle);
	glDeleteShader(VertHandle);
	glDeleteShader(FragHandle);

	// Create buffers
	glGenBuffers(1, &g_VboHandle);
	glBindBuffer(GL_ARRAY_BUFFER, g_VboHandle);
	glObjectLabel(GL_BUFFER, g_VboHandle, -1, "Dear ImGui VBO");
	glGenBuffers(1, &g_ElementsHandle);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, g_ElementsHandle);
	glObjectLabel(GL_BUFFER, g_ElementsHandle, -1, "Dear ImGui EBO");

	ImGui_ImplOpenGL3_CreateFontsTexture();
	return true;
}

void ImGui_ImplOpenGL3_DestroyDeviceObjects()
{
	glDeleteVertexArrays(1, &g_vertex_array_object);
	glDeleteBuffers(1, &g_VboHandle);
	g_VboHandle = 0;
	glDeleteBuffers(1, &g_ElementsHandle);
	g_ElementsHandle = 0;
	glDeleteProgram(g_ShaderHandle);
	g_ShaderHandle = 0;

	ImGui_ImplOpenGL3_DestroyFontsTexture();
}

#include "imgui/imgui.h"
#include "imgui_impl_opengl4.h"
#include <SFML/Window.hpp>
#include <chrono>
#include "pgl.hpp"
#include "imgui_sfml_impl.hpp"

int main()
{
	sf::ContextSettings settings(0, 0, 16, 4, 5, sf::ContextSettings::Core);
	sf::Window window(sf::VideoMode(1000, 800), "guifuck", sf::Style::Default, settings);
	window.setFramerateLimit(150);
	if (!gladLoadGL())
	{
		std::cerr << "Failed to initialize OpenGL loader!\n";
		return 1;
	}
	glEnable(GL_DEBUG_OUTPUT);
	glDebugMessageCallback(pgl::MessageCallback, nullptr);

	// Setup Dear ImGui context
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();

	// Setup Dear ImGui style
	ImGui::StyleColorsDark();
	//ImGui::StyleColorsClassic();
	ImGui_ImplSFML_init();
	ImGui_ImplOpenGL3_Init();
	// Our state
	bool show_demo_window = true;
	bool show_another_window = false;
	ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);

	// Main loop
	bool done = false;
	auto start = std::chrono::steady_clock::now();
	ImGuiIO& io = ImGui::GetIO();
	io.DisplaySize = {1000, 800};
	while (!done)
	{
		const auto endtp = std::chrono::steady_clock::now();
		io.DeltaTime = std::chrono::duration_cast<std::chrono::duration<float, std::ratio<1, 1>>>(endtp - start).count();
		start = endtp;
		// Poll and handle events (inputs, window resize, etc.)
		// You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
		// - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application.
		// - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application.
		// Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.
		sf::Event event;
		while (window.pollEvent(event))
		{
			ImGui_ImplSFML_ProcessEvent(event);
			switch(event.type)
			{
				case sf::Event::Closed:
					done = true;
					break;
				case sf::Event::MouseMoved:
					if(io.WantCaptureMouse)
					{
						break;
					}
					break;
				case sf::Event::Resized:
					glViewport(0, 0, static_cast<int>(event.size.width), static_cast<int>(event.size.height));
				default:
					break;
			}
		}
		// Start the Dear ImGui frame
		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplSFML_NewFrame(window);
		ImGui::NewFrame();

		// 1. Show the big demo window (Most of the sample code is in ImGui::ShowDemoWindow()! You can browse its code to learn more about Dear ImGui!).
		if (show_demo_window)
			ImGui::ShowDemoWindow(&show_demo_window);

		// 2. Show a simple window that we create ourselves. We use a Begin/End pair to created a named window.
		{
			static float f = 0.0f;
			static int counter = 0;
			static int fps = 150;

			ImGui::Begin("Hello, world!", nullptr, ImGuiWindowFlags_AlwaysAutoResize);                          // Create a window called "Hello, world!" and append into it.

			ImGui::Text("This is some useful text.");               // Display some text (you can use a format strings too)
			ImGui::Checkbox("Demo Window", &show_demo_window);      // Edit bools storing our window open/close state
			ImGui::Checkbox("Another Window", &show_another_window);

			ImGui::SliderFloat("float", &f, 0.0f, 1.0f);            // Edit 1 float using a slider from 0.0f to 1.0f
			ImGui::ColorEdit3("clear color", reinterpret_cast<float*>(&clear_color)); // Edit 3 floats representing a color
			if(ImGui::SliderInt("Framerate Limit", &fps, 10, 150))
				window.setFramerateLimit(static_cast<unsigned int>(fps));

			if (ImGui::Button("Button"))                            // Buttons return true when clicked (most widgets return true when edited/activated)
				counter++;
			ImGui::SameLine();
			ImGui::Text("counter = %d", counter);

			ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0 / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
			ImGui::End();
		}

		// 3. Show another simple window.
		if (show_another_window)
		{
			ImGui::Begin("Another Window", &show_another_window);   // Pass a pointer to our bool variable (the window will have a closing button that will clear the bool when clicked)
			ImGui::Text("Hello from another window!");
			if (ImGui::Button("Close Me"))
				show_another_window = false;
			ImGui::End();
		}

		// Rendering
		ImGui::Render();
		glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
		glClear(GL_COLOR_BUFFER_BIT);
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
		window.display();
	}

	// Cleanup
	ImGui_ImplOpenGL3_Shutdown();
	ImGui::DestroyContext();

	return 0;
}
